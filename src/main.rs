use axum::{
    extract::Path,
    http::{header, HeaderMap, StatusCode},
    response::{Html, IntoResponse},
    routing::{get},
    Router,
};
use askama_axum::Template;

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate{ }

#[tokio::main]
async fn main() {
    let app = Router::new()
        .route("/", get(handle_main))
        .route("/_assets/*path", get(handle_assets));

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3999")
        .await
        .unwrap();

    println!("listening on {}", listener.local_addr().unwrap());
    axum::serve(listener, app).await.unwrap();
}

async fn handle_main() -> impl IntoResponse {
    let template = IndexTemplate { };
    let reply_html = template.render().unwrap();
    (StatusCode::OK, Html(reply_html).into_response())
}

static THEME_CSS: &str = include_str!("../assets/index.css");
static FAVICON: &str = include_str!("../assets/favicon.svg");

async fn handle_assets(Path(path): Path<String>) -> impl IntoResponse {
    let mut headers = HeaderMap::new();

    if path == "index.css" {
        headers.insert(header::CONTENT_TYPE, "text/css".parse().unwrap());
        (StatusCode::OK, headers, THEME_CSS)
    } else if path == "favicon.svg" {
        (StatusCode::OK, headers, FAVICON)
    } else {
        (StatusCode::NOT_FOUND, headers, "")
    }
}